# USWDS Templates

## Overview
This module provides a single source of USWDS component templates that can be used by any module or theme.

Each template contains a comment block listing the variables:
```
* USWDS template variables:
* - accordion_multi: boolean: 0 / 1
* - accordion_border: boolean: 0 / 1
* - accordion_items: renderable array of usa-accordion items
* - classes: classes array (inherits base template classes as default)
```
It also lists an example to show how to include it in your own templates:
```
* Usage example:
* {% include '@uswds_templates/usa-accordion.html.twig' with {
*   'accordion_multi': var_accordion_multi,
*   'accordion_border': var_accordion_border,
*   'accordion_items': var_accordion_item | field_value,
*   'classes': var_classes
* } %}
```

## Installation
Install this module as usual using Composer:
```
composer require drupal/uswds_templates
```

## Demoing components or prototyping
This module also provides a set of demo TWIG templates that can be used for demoing components or prototyping with default content. These templates can be included like so:
```
{% include '@uswds_templates/demo/usa-banner.html.twig' %}
```
